variables:
    # When using dind service we need to instruct docker, to talk with the
    # daemon started inside of the service. The daemon is available with
    # a network connection instead of the default /var/run/docker.sock socket.
    #
    # The 'docker' hostname is the alias of the service container as described at
    # https://docs.gitlab.com/ee/ci/docker/using_docker_images.html#accessing-the-services
    #
    # Note that if you're using Kubernetes executor, the variable should be set to
    # tcp://localhost:2375 because of how Kubernetes executor connects services
    # to the job container
    DOCKER_HOST: tcp://docker:2375/
    # When using dind, it's wise to use the overlayfs driver for
    # improved performance.
    DOCKER_DRIVER: overlay2
    CONTAINER_IMAGE: $CI_REGISTRY_IMAGE
    CONTAINER_TAG: $CI_PIPELINE_IID-$CI_COMMIT_SHORT_SHA
    DOCKER_IMAGE: $CI_REGISTRY_IMAGE:$CI_PIPELINE_IID-$CI_COMMIT_SHORT_SHA
    DOCKER_ASPNETCORE_IMAGE: mcr.microsoft.com/dotnet/core/sdk:3.1.102-bionic
    MIGRATOR_CONTAINER_IMAGE: $CI_REGISTRY_IMAGE/migrator
    SONAR_USER_HOME: "${CI_PROJECT_DIR}/.sonar"  # Defines the location of the analysis task cache
    GIT_DEPTH: "0"  # Tells git to fetch all the branches of the project, required by the analysis task

stages:
    - build
    - test
    - package
    - containerize
    - container_scanning
    - deploy

build:client:
    image: node:lts-alpine
    stage: build
    except:
        - releases/firstplayable
    script:
        - cd html
        - npm install
        - npm run build
    artifacts:
        name: "client-$CI_COMMIT_REF_SLUG-$CI_COMMIT_SHORT_SHA"
        paths:
            - html/dist/
        expire_in: 1h
    cache:
        key: "$CI_COMMIT_REF_SLUG"
        paths:
            - html/node_modules/
            - html/package-lock.json

build:server:
    image: $DOCKER_ASPNETCORE_IMAGE
    stage: build
    script:
        - dotnet restore --packages .nuget
        - dotnet build -c Release --no-restore --packages .nuget
    artifacts:
        name: "server-$CI_COMMIT_REF_SLUG-$CI_COMMIT_SHORT_SHA"
        paths:
            - server/src/*/bin
            - server/src/MythicTable/obj/project.assets.json
            - server/tests/*/bin
        expire_in: 1h
    cache:
        key: "$CI_COMMIT_REF_SLUG"
        paths:
            - .nuget/

test:client:
    stage: test
    image: node:lts-alpine
    script:
        - cd html
        - npm install
        - npm run test -- --coverage
    artifacts:
        name: "client-coverage-$CI_COMMIT_REF_SLUG-$CI_COMMIT_SHORT_SHA"
        paths:
            - html/coverage/
        reports:
            cobertura: html/coverage/cobertura-coverage.xml
    cache:
        key: "$CI_COMMIT_REF_SLUG"
        paths:
            - html/node_modules/
            - html/package-lock.json

test:server:
    stage: test
    image: $DOCKER_ASPNETCORE_IMAGE
    dependencies:
        - build:server
    needs:
        - build:server
    script:
        - dotnet test --collect:"XPlat Code Coverage"
    artifacts:
      name: "server-coverage-$CI_COMMIT_REF_SLUG-$CI_COMMIT_SHORT_SHA"
      reports:
        cobertura: server/tests/*/TestResults/*/coverage.cobertura.xml
      paths:
        - server/tests/MythicTable.Integration.Tests/TestResults/*
        - server/tests/MythicTable.Tests/TestResults/*

sonarcloud-check:
    stage: deploy
    dependencies:
        - test:server
        - test:client
    image:
        name: sonarsource/sonar-scanner-cli:latest
        entrypoint: [""]
    cache:
        key: "${CI_JOB_NAME}"
        paths:
          - .sonar/cache
    script:
        - sonar-scanner -Dproject.settings="frontend-scanner.properties"
    only:
        - merge_requests
        - main
        - fp
