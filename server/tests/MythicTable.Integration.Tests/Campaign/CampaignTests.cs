using System.Threading.Tasks;
using System.Net;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using MythicTable.Campaign.Data;
using MythicTable.Integration.TestUtils.Util;
using MythicTable.TestUtils.Profile.Util;
using Newtonsoft.Json;
using Xunit;

namespace MythicTable.Integration.Tests.Campaign
{
    public class CampaignTests
    {
        [Fact]
        public async Task CreateCampaignTest()
        {
            var builder = new WebHostBuilder().UseStartup<TestStartup>();
            var server = new TestServer(builder);
            var client = server.CreateClient();

            await ProfileTestUtil.Login(client);

            var campaign = new CampaignDTO()
            {
                Name = "Integration Test Campaign"
            };

            using var response = await RequestHelper.PostStreamAsync(client, "/api/campaigns", campaign);
            response.EnsureSuccessStatusCode();
            Assert.Equal("application/json; charset=utf-8", response.Content.Headers.ContentType.ToString());
            var json = await response.Content.ReadAsStringAsync();
            var newCampaign = JsonConvert.DeserializeObject<CampaignDTO>(json);
            Assert.Equal("Integration Test Campaign", newCampaign.Name);
        }

        [Fact]
        public async Task AuthorizedCampaignDeleteTest() {
            var builder = new WebHostBuilder().UseStartup<TestStartup>();
            var server = new TestServer(builder);
            var client = server.CreateClient();

            await ProfileTestUtil.Login(client);

            var campaign = new CampaignDTO()
            {
                Name = "Integration Test Campaign"
            };

            using var response = await RequestHelper.PostStreamAsync(client, "/api/campaigns", campaign);
            response.EnsureSuccessStatusCode();
            var json = await response.Content.ReadAsStringAsync();
            var campaignObject = JsonConvert.DeserializeObject<CampaignDTO>(json);

            using var deleteResponse = await RequestHelper.DeleteStreamAsync(client, $"/api/campaigns/{campaignObject.Id}");
            deleteResponse.EnsureSuccessStatusCode();

            using var getResponse = await RequestHelper.GetStreamAsync(client, $"/api/campaigns/{campaignObject.Id}");
            Assert.Equal(HttpStatusCode.NotFound, getResponse.StatusCode);
        }
    }
}