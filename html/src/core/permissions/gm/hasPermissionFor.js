import _ from 'lodash';

const permissionsVerification = {
    namespaced: true,
    getters: {
        hasPermissionFor: (state, getters, rootState, rootGetters) => (specifiedPermission, id = '') => {
            const gameMasterControlActive = rootGetters['gmPermissions/permissionSettings'][specifiedPermission];
            if (gameMasterControlActive) {
                return gmPermissionActiveFor[specifiedPermission]({ getters, id });
            } else {
                return assumedPermissionFor[specifiedPermission]({ getters, id });
            }
        },
        getGameMasterStatus: (state, getters, rootState, rootGetters) => {
            return rootGetters['players/isGameMaster'](getters.currentUserId);
        },
        currentUserId(state, getters, rootState) {
            return rootState.profile.me.id;
        },
        isMapOwner: (state, getters) => mapUserId => {
            return getters.currentUserId === mapUserId;
        },
        isActiveMapOwner(state, getters, rootState) {
            if (!_.isEmpty(rootState.gamestate.activeMap)) {
                return getters.currentUserId === rootState.gamestate.activeMap._userid;
            } else {
                return false;
            }
        },
        permissionActiveEditAnyMap: (state, getters) => mapUserId => {
            return getters.getGameMasterStatus || getters.isMapOwner(mapUserId);
        },
    },
};

const gmPermissionActiveFor = {
    gridControl: ({ getters }) => getters.getGameMasterStatus,
    fogControl: ({ getters }) => getters.getGameMasterStatus,
    editAnyMap: ({ getters, id }) => getters.permissionActiveEditAnyMap(id),
    moveAll: ({ getters }) => getters.getGameMasterStatus,
};

const assumedPermissionFor = {
    gridControl: ({ getters }) => getters.isActiveMapOwner,
    fogControl: ({ getters }) => getters.isActiveMapOwner,
    editAnyMap: ({ getters, id }) => getters.isMapOwner(id),
    moveAll: ({ getters, id }) => getters.isMapOwner(id),
};

export default permissionsVerification;
