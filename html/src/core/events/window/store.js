const state = {
    windowSize: '',
};

const mutations = {
    updateWindowSize(state, windowSize) {
        state.windowSize = windowSize;
    },
};

let trackWindowResize;

const actions = {
    windowResize({ commit }) {
        commit('updateWindowSize', window.innerWidth);
    },
    initiateFunctions({ dispatch }) {
        dispatch('windowResize');
        trackWindowResize = () => dispatch('windowResize');
    },
    addTracking({ dispatch }) {
        dispatch('initiateFunctions');
        window.addEventListener('resize', trackWindowResize);
    },
    removeTracking() {
        window.removeEventListener('resize', trackWindowResize);
    },
};

const WindowStore = {
    namespaced: true,
    state,
    mutations,
    actions,
};

export default WindowStore;
