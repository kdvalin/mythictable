import { mount } from '@vue/test-utils';

import HeaderTutorial from '@/table/components/play-view/Header/HeaderTutorial.vue';

let wrapper;
function buildWrapper() {
    wrapper = mount(HeaderTutorial);
}

describe('HeaderTutorial', () => {
    beforeEach(buildWrapper);
    it('Renders as itself.', () => {
        expect(wrapper.findComponent(HeaderTutorial).exists()).toBeTruthy();
    });
    it('Emits when close button is clicked', () => {
        wrapper.find('.modal-button').trigger('click');
        expect(wrapper.emitted().close.length).toBe(1);
    });
});
