import ErrorDialog from '@/common/components/ErrorDialog.vue';
import { mount } from '@vue/test-utils';

describe('ErrorDialog', () => {
    it('should not render if message is empty', () => {
        const wrapper = mount(ErrorDialog, {
            computed: {
                message: () => '',
            },
        });
        expect(wrapper.contains('.container')).toBeFalsy();
    });
    it('should render if message is not empty', () => {
        const wrapper = mount(ErrorDialog, {
            computed: {
                message: () => 'something',
            },
        });
        expect(wrapper.contains('.container')).toBeTruthy();
        expect(wrapper.find('.message').text()).toMatch('something');
    });
});
