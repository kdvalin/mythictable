import BaseModal from '@/core/components/BaseModal.vue';
import { mount } from '@vue/test-utils';

describe('BaseModal', () => {
    it('should render if show is false', () => {
        const wrapper = mount(BaseModal, {
            propsData: {
                show: false,
            },
        });
        expect(wrapper.find('.modal-mask').isVisible()).toBeTruthy();
    });
    it('should render if show is true', () => {
        const wrapper = mount(BaseModal, {
            propsData: {
                show: true,
            },
        });
        expect(wrapper.find('.modal-mask').isVisible()).toBeTruthy();
    });
    it('should render slot contents', () => {
        const wrapper = mount(BaseModal, {
            slots: {
                default: 'foobar',
            },
        });
        expect(wrapper.find('.modal-mask').text()).toMatch('foobar');
    });
});
