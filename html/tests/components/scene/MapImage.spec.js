import 'jest-canvas-mock';

import { createLocalVue, shallowMount } from '@vue/test-utils';

import MapImage from '@/table/components/scene/MapImage.vue';

const localVue = createLocalVue();

describe('MapImage', () => {
    describe('imageConfig', () => {
        it('should not throw an error', () => {
            shallowMount(MapImage, {
                localVue,
            });
        });
    });
});
