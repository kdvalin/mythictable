#!/bin/bash
dotnet sonarscanner begin /k:kdvalin_mythictable /o:kdvalin /d:sonar.login="$SONARCLOUD_TOKEN" /d:sonar.host.url="https://sonarcloud.io" /d:sonar.cs.dotcover.reportsPaths="dotcover.html" /d:sonar.sources="./server"
dotnet build
dotnet dotcover test --dcReportType=HTML --dcOutput=dotcover.html --dcFilters="+:MythicTable*;"
dotnet sonarscanner end /d:sonar.login="$SONARCLOUD_TOKEN"

